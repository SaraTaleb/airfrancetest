package com.sara.technicaltest.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.sara.technicaltest.web.configuration.TechnicalTestWebConfiguration;

@SpringBootApplication
@Import(TechnicalTestWebConfiguration.class)
public class TechnicalTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechnicalTestApplication.class, args);
	}

}

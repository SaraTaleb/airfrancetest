package com.sara.technicaltest.business.configuration;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import lombok.extern.slf4j.Slf4j;

/**
 * Class for logging method input and output
 */
@Aspect
@Slf4j
//@ConditionalOnProperty(value = "log.aop.enabled", havingValue = "true", matchIfMissing = false)
public class LoggingAspect {

	@Around("@annotation(LogExecutionTime)")
//	 @Around("@annotation(com.sara.technicaltest.business.configuration.LogExecutionTime)")
	public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
		
//		if (log.isDebugEnabled()) {
			final StringBuilder stb = new StringBuilder("[start][" + joinPoint.getSignature() + "] ");
			if (joinPoint.getArgs().length == 0 ) {
				stb.append("no parameters");
			} else {
				for(int i = 0 ; i < joinPoint.getArgs().length ; i++) {
					final Object value = joinPoint.getArgs()[i];
					if(value == null) {
						stb.append("null").append(" ");
					} else {
						stb.append(joinPoint.getArgs()[i].toString()).append(" ");
					}

				}
			}
			log.debug(stb.toString());
//		}
		
		final long startTime = System.currentTimeMillis();

		final Object proceed = joinPoint.proceed();

		final long endTime = System.currentTimeMillis();
		
		final long executionTime = endTime - startTime;

//		if (log.isDebugEnabled()) {
		log.debug("[end][{}] executed in {} ms", joinPoint.getSignature(), executionTime);
//	}

		return proceed;
	}
}

package com.sara.technicaltest.business.exception;

public class UserAlreadyExistsException extends IllegalArgumentException {

	private static final long serialVersionUID = 1L;

	public UserAlreadyExistsException(String message) {
		super(message);
	}
	
}
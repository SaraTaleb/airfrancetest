package com.sara.technicaltest.business.exception;

public class UserNotValidException extends IllegalArgumentException {

	private static final long serialVersionUID = 1L;

	public UserNotValidException(String message) {
		super(message);
	}
	
}

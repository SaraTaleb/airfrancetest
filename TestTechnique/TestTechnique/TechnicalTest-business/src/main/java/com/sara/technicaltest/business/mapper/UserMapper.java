package com.sara.technicaltest.business.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import com.sara.technicaltest.business.dto.UserDto;
import com.sara.technicaltest.data.entity.UserEntity;

/**
 * Mapping dto to entity and entity to dto
 */
@Mapper(componentModel = "spring")
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mappings({
//        @Mapping(target = "idUser", source = "idUser"),
        @Mapping(target = "firstName", source = "firstName"),
        @Mapping(target = "lastName", source = "lastName"),
        @Mapping(target = "email", source = "email"),
        @Mapping(target = "birthdate", source = "birthdate"),
        @Mapping(target = "phoneNumber", source = "phoneNumber"),
        @Mapping(target = "gender", source = "gender"),
        @Mapping(target = "countryOfResidence", source = "countryOfResidence")
    })
    UserDto entityToDto(UserEntity userEntity);

    @Mappings({
//        @Mapping(target = "idUser", source = "idUser"),
        @Mapping(target = "firstName", source = "firstName"),
        @Mapping(target = "lastName", source = "lastName"),
        @Mapping(target = "email", source = "email"),
        @Mapping(target = "birthdate", source = "birthdate"),
        @Mapping(target = "phoneNumber", source = "phoneNumber"),
        @Mapping(target = "gender", source = "gender"),
        @Mapping(target = "countryOfResidence", source = "countryOfResidence")
    })
    UserEntity dtoToEntity(UserDto userDto);
}
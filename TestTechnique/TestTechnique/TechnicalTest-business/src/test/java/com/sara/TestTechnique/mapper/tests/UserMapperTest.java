package com.sara.TestTechnique.mapper.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.sara.technicaltest.business.dto.UserDto;
import com.sara.technicaltest.business.mapper.UserMapper;
import com.sara.technicaltest.business.mapper.UserMapperImpl;
import com.sara.technicaltest.data.entity.UserEntity;


@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {UserMapperImpl.class})
public class UserMapperTest {

	private final UserMapper userMapper = Mappers.getMapper(UserMapper.class);
	
	private UserEntity userEntity;
	private UserDto userDto;
	
	@BeforeEach
	void init() {
		final Long userId = 1L;
		final LocalDate birthdateTime = LocalDate.of(2000, 12, 31);

		userEntity = UserEntity.builder().idUser(userId).firstName("Sara").lastName("TALEB").email("sara.taleb@alitya.com").birthdate(birthdateTime).phoneNumber("0711223344").gender("Female").countryOfResidence("France").build();
		userDto = new UserDto(userId, "Sara", "TALEB", "sara.taleb@alitya.com", birthdateTime, "0711223344", "Female", "France");


	}
	
	@Test
	@DisplayName("Test de la méthode qui permet de convertir entity to dto")
	void entityToDto() {
		final UserDto resultat = userMapper.entityToDto(userEntity);
		assertEquals(resultat.getFirstName(), userDto.getFirstName());
		assertEquals(resultat.getLastName(), userDto.getLastName());
		assertEquals(resultat.getEmail(), userDto.getEmail());
	}
	
	
	@Test
	@DisplayName("Test de la méthode qui permet de convertir entity to dto")
	void dtoToEntity() {
		final UserEntity resultat = userMapper.dtoToEntity(userDto);
		assertEquals(resultat.getFirstName(), userEntity.getFirstName());
		assertEquals(resultat.getLastName(), userEntity.getLastName());
		assertEquals(resultat.getEmail(), userEntity.getEmail());
	}
}

package com.sara.technicaltest.data.entity;

import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * User Entity
 */
@Document(collection = "users")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserEntity {
	
    public static final String SEQUENCE_NAME = "users_sequence";
	
    /** User ID*/
    @Id
    private Long idUser;

    /** firstName of user. Mandatory field */
    private String firstName;

    /** lastName of user. Mandatory field */
    private String lastName;

    /** email adress of user. Mandatory and unique field */
    private String email;

    /** birthdate of user. Mandatory field */
    private LocalDate birthdate;

    /** phoneNumber of user. Optional field */
    private String phoneNumber;

    /** gender of user. Optional field  */
    private String gender;

    /** country of user. Mandatory field */
    private String countryOfResidence;
}

package com.sara.technicaltest.data.utils;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Document(collection = "userdatabase_sequences")
public class DatabaseSequence {

    @Id
    private String id;

    private long seq;

}

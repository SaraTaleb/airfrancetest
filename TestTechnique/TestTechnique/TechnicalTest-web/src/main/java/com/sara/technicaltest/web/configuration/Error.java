package com.sara.technicaltest.web.configuration;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Error {

	private String message;
}

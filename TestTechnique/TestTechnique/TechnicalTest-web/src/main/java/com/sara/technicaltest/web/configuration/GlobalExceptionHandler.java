package com.sara.technicaltest.web.configuration;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.sara.technicaltest.business.exception.UserAlreadyExistsException;
import com.sara.technicaltest.business.exception.UserNotValidException;


@ControllerAdvice
public class GlobalExceptionHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

	
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Error> handleUserDtoValidationExceptions(MethodArgumentNotValidException exception) {
        BindingResult result = exception.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();
        
        // Construire un message d'erreur à partir des erreurs de validation
        StringBuilder errorMessage = new StringBuilder("Validation failed for fields: ");
        for (FieldError fieldError : fieldErrors) {
            errorMessage.append(fieldError.getField()).append(" - ").append(fieldError.getDefaultMessage()).append("; ");
            
        }
        
        LOGGER.error("Erreur de validation du DTO : {}", errorMessage.toString());

        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(Error.builder().message(errorMessage.toString()).build());
    }
    
    
	@ExceptionHandler({ UserNotValidException.class})
	public ResponseEntity<Error> handlerUserNotValidException(UserNotValidException exception) {
		
		LOGGER.info("User validation error", exception.getMessage());
		return new ResponseEntity<>(Error.builder().message(exception.getMessage()).build(), HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(UserAlreadyExistsException.class)
	public ResponseEntity<Error> handlerUserAlreadyExists(UserAlreadyExistsException exception) {
		
		LOGGER.info("Existing user", exception.getMessage());
		return new ResponseEntity<>(Error.builder().message(exception.getMessage()).build(), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Error> handleException(Exception exception) {
		
		LOGGER.error("Internal server error occurred", exception);
		return new ResponseEntity<>(Error.builder().message("An unexpected error occurred: " + exception.getMessage()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
	}
}

package com.sara.technicaltest.controller.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.sara.technicaltest.business.dto.UserDto;
import com.sara.technicaltest.business.service.UserService;
import com.sara.technicaltest.web.controller.UserController;


@ExtendWith(MockitoExtension.class)
@DisplayName("Controller unit tests for the user entity")
@Tag("Unitaires")
public class UserControllerTest {

    @InjectMocks
    private UserController userController;
    
    @Mock
    private UserService userService;
    
    @Test
    @DisplayName("Test the method used to get a user")
    public void getUser() {
        Long userId = 1L;
		final LocalDate birthdateTime = LocalDate.of(2000, 12, 31);

        UserDto userDto = new UserDto(userId, "Sara", "TALEB", "sara.taleb@alithya.com", birthdateTime, "0711223344", "Female", "France");
        when(userService.getUserById(userId)).thenReturn(userDto);
        
        ResponseEntity<UserDto> response = userController.getUser(userId);
        
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        UserDto result = response.getBody();
        assertNotNull(result);
        assertEquals(userId, result.getIdUser());
        assertEquals("Sara", result.getFirstName());
        assertEquals("TALEB", result.getLastName());
        
        verify(userService).getUserById(userId);
    }
    
    @Test
    @DisplayName("Test to recover a non-existent user")
    public void getUser_nonExistingUser() {
        Long userId = 1L;
        when(userService.getUserById(userId)).thenReturn(null);

        ResponseEntity<UserDto> response = userController.getUser(userId);

        assertNotNull(response);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());

        verify(userService).getUserById(userId);
    }
    
    @Test
    @DisplayName("Testing the method used to create a user")
    public void saveUser() {
        final Long userId = 1L;
		final LocalDate birthdateTime = LocalDate.of(2000, 12, 31);

		UserDto user = UserDto.builder()
//                .idUser(userId)
                .firstName("Sara")
                .lastName("TALEB")
                .email("sara.taleb@alithya.com")
                .birthdate(birthdateTime)
                .phoneNumber("0711223344")
                .gender("Female")
                .countryOfResidence("France")
                .build();
                
        UserDto userDto = new UserDto(userId, "Sara", "TALEB", "sara.taleb@alithya.com", birthdateTime, "0711223344", "Female", "France");
        
        when(userService.addUser(Mockito.any(UserDto.class))).thenReturn(user);
        
        ResponseEntity<UserDto> response = userController.addUser(userDto);

        assertNotNull(response);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        UserDto resultDto = response.getBody();
        assertNotNull(resultDto);
        assertEquals(user.getFirstName(), resultDto.getFirstName());
        assertEquals(user.getLastName(), resultDto.getLastName());
        assertEquals(user.getEmail(), resultDto.getEmail());
        assertEquals(user.getBirthdate(), resultDto.getBirthdate());
        assertEquals(user.getPhoneNumber(), resultDto.getPhoneNumber());
        assertEquals(user.getGender(), resultDto.getGender());
        assertEquals(user.getCountryOfResidence(), resultDto.getCountryOfResidence());
     
        verify(userService).addUser(Mockito.any(UserDto.class));
    }
}
